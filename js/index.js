$(function(){
    $('[data-toggle="popover"]').popover()
    $("[data-toggle='tooltip']").tooltip()
    $('.carousel').carousel({
        interval: 100
    });
    $('#contacto').on('show.bs.modal', function(e){
        console.log('el modal se está mostrando');
    });
    $('#contacto').on('shown.bs.modal', function(e){
        console.log('el modal se mostró');
        console.log('el modal se está mostrando');
        $('#suscribirseBtn').removeClass('btn-outline-success');
        $('#suscribirseBtn').addClass('btn-primary');
        $('#suscribirseBtn').prop('disabled',true);

    });
    $('#contacto').on('hide.bs.modal', function(e){
        console.log('el modal se está ocultando');
    });
    $('#contacto').on('hidden.bs.modal', function(e){
        console.log('el modal se ocultó');
        $('#suscribirseBtn').removeClass('btn-primary');
        $('#suscribirseBtn').addClass('btn-outline-success');
        $('#suscribirseBtn').prop('disabled',false);

    });
});